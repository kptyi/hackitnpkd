(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-agenda-creation *ngIf=\"!api.state.showMeeting\"></app-agenda-creation>\r\n<div class=\"main\" *ngIf=\"api.state.showMeeting\">\r\n\t<div class=\"top\">\r\n\t\t<app-main-header></app-main-header>\r\n\t\t<button (click)=\"raiseEvent()\">Raise event</button>\r\n\t</div>\r\n\r\n\t<div class=\"bottom\">\r\n\t\t<app-agenda></app-agenda>\r\n\t\t<app-whiteboard></app-whiteboard>\r\n\t\t<app-chat-box></app-chat-box>\r\n\t\t<app-users></app-users>\r\n\t</div>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/transmiter.service */ "./src/app/services/transmiter.service.ts");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service/api.service */ "./src/app/service/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(transmiter, api) {
        this.transmiter = transmiter;
        this.api = api;
        this.title = 'my-sassy-app';
        this.showMeeting = this.api.state.showMeeting;
        transmiter.broadcastChannel$.subscribe(function (value) {
            console.log('AppComponent: ' + value);
        });
    }
    AppComponent.prototype.raiseEvent = function () {
        this.transmiter.broadcast('Ok go');
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")],
            providers: [_services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__["TransmiterService"]]
        }),
        __metadata("design:paramtypes", [_services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__["TransmiterService"], _service_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_chat_box_chat_box_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/chat-box/chat-box.component */ "./src/app/components/chat-box/chat-box.component.ts");
/* harmony import */ var _components_main_header_main_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/main-header/main-header.component */ "./src/app/components/main-header/main-header.component.ts");
/* harmony import */ var _components_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/agenda/agenda.component */ "./src/app/components/agenda/agenda.component.ts");
/* harmony import */ var _components_whiteboard_whiteboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/whiteboard/whiteboard.component */ "./src/app/components/whiteboard/whiteboard.component.ts");
/* harmony import */ var _components_users_users_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/users/users.component */ "./src/app/components/users/users.component.ts");
/* harmony import */ var _components_kiki_rage_kiki_rage_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/kiki-rage/kiki-rage.component */ "./src/app/components/kiki-rage/kiki-rage.component.ts");
/* harmony import */ var _components_agenda_creation_agenda_creation_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/agenda-creation/agenda-creation.component */ "./src/app/components/agenda-creation/agenda-creation.component.ts");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var _components_card_code_card_code_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/card-code/card-code.component */ "./src/app/components/card-code/card-code.component.ts");
/* harmony import */ var _components_card_drawing_card_drawing_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/card-drawing/card-drawing.component */ "./src/app/components/card-drawing/card-drawing.component.ts");
/* harmony import */ var _components_card_text_card_text_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/card-text/card-text.component */ "./src/app/components/card-text/card-text.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _components_chat_box_chat_box_component__WEBPACK_IMPORTED_MODULE_4__["ChatBoxComponent"],
                _components_main_header_main_header_component__WEBPACK_IMPORTED_MODULE_5__["MainHeaderComponent"],
                _components_agenda_agenda_component__WEBPACK_IMPORTED_MODULE_6__["AgendaComponent"],
                _components_whiteboard_whiteboard_component__WEBPACK_IMPORTED_MODULE_7__["WhiteboardComponent"],
                _components_users_users_component__WEBPACK_IMPORTED_MODULE_8__["UsersComponent"],
                _components_kiki_rage_kiki_rage_component__WEBPACK_IMPORTED_MODULE_9__["KikiRageComponent"],
                _components_agenda_creation_agenda_creation_component__WEBPACK_IMPORTED_MODULE_10__["AgendaCreationComponent"],
                _components_card_code_card_code_component__WEBPACK_IMPORTED_MODULE_12__["CardCodeComponent"],
                _components_card_drawing_card_drawing_component__WEBPACK_IMPORTED_MODULE_13__["CardDrawingComponent"],
                _components_card_text_card_text_component__WEBPACK_IMPORTED_MODULE_14__["CardTextComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]
            ],
            providers: [_service_api_service__WEBPACK_IMPORTED_MODULE_11__["ApiService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/agenda-creation/agenda-creation.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/agenda-creation/agenda-creation.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal\">\r\n\t<div class=\"modal-header\">\r\n\t\t<div class=\"modal-header-left\">\r\n\t\t\t<div class=\"modal-subname\">Smart office</div>\r\n\t\t\t<div class=\"modal-name\">Create new meeting</div>\r\n\t\t</div>\r\n\r\n\t\t<img src=\"./assets/img/logo.svg\"/>\r\n\t</div>\r\n\t<div class=\"modal-body\">\r\n\t\t<div class=\"modal-left modal-block\">\r\n\t\t\t<div class=\"agenda-header\">Meeting details:</div>\r\n\t\t\t<div class=\"element-wrapper\">\r\n\t\t\t\t<label class=\"element-label\">Meeting Name:</label>\r\n\t\t\t\t<input type=\"text\" class=\"input-element\" />\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"modal-mid modal-block\">\r\n\t\t\t<div class=\"agenda-header\">Add agenda items:</div>\r\n\t\t\t<div class=\"element-wrapper\">\r\n\t\t\t\t<label class=\"element-label\">Agenda item name:</label>\r\n\t\t\t\t<input type=\"text\" class=\"input-element\" name=\"Name\" id=\"aName\" [(ngModel)]=\"name\" [placeholder]=\"\" />\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"element-wrapper\">\r\n\t\t\t\t<label class=\"element-label\">Agenda item description:</label>\r\n\t\t\t\t<textarea class=\"input-element\" name=\"Description\" id=\"aDescription\" [(ngModel)]=\"description\" [placeholder]=\"\"></textarea>\r\n\t\t\t</div>\r\n\r\n\t\t\t<button (click)=\"addAgenda()\" class=\"my-btn\">Add Agenda</button>\r\n\r\n\r\n\t\t</div>\r\n\r\n\t\t<div class=\"modal-right modal-block\">\r\n\t\t\t<div class=\"agenda-header\">Agenda:</div>\r\n\t<!-- \t\t<ul>\r\n\t\t\t\t<li *ngFor=\"let agenda of agendas;\">\r\n\t\t\t\t\t{{ agenda.name }}\r\n\t\t\t\t</li>\r\n\t\t\t\t{{agendas | json}}\r\n\t\t\t</ul> -->\r\n\r\n\t\t\t<div class=\"agenda-list\">\r\n\t\t\t\t<div *ngFor=\"let agenda of agendas; index as i\" class=\"agenda\">\r\n\t\t\t\t\t<div class=\"agenda-name\">{{ i + 1 }}. {{agenda.name}}</div>\r\n\t\t\t\t\t<div class=\"agenda-description\">{{agenda.description}}</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\r\n\t\t\t<button class=\"my-btn start-meeting-btn\" (click)=\"next()\">Start Meeting</button>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/agenda-creation/agenda-creation.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/agenda-creation/agenda-creation.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWdlbmRhLWNyZWF0aW9uL2FnZW5kYS1jcmVhdGlvbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/agenda-creation/agenda-creation.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/agenda-creation/agenda-creation.component.ts ***!
  \*************************************************************************/
/*! exports provided: AgendaCreationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaCreationComponent", function() { return AgendaCreationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var _model_Model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/Model */ "./src/app/model/Model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AgendaCreationComponent = /** @class */ (function () {
    function AgendaCreationComponent(api) {
        this.state = api.state;
        console.log(this.state);
        console.log('KK');
        this.agendas = [];
    }
    AgendaCreationComponent.prototype.ngOnInit = function () {
    };
    AgendaCreationComponent.prototype.addAgenda = function () {
        this.agendas.push(new _model_Model__WEBPACK_IMPORTED_MODULE_2__["Agenda"](this.name, this.description, this.duration));
        this.name = '';
        this.duration = null;
        this.description = '';
    };
    AgendaCreationComponent.prototype.next = function () {
        var _this = this;
        this.agendas.forEach(function (agenda) {
            _this.state.agenda.push(agenda);
        });
        if (this.state.agenda.length > 0) {
            this.state.agenda[0].active = true;
        }
        this.state.showMeeting = true;
    };
    AgendaCreationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-agenda-creation',
            template: __webpack_require__(/*! ./agenda-creation.component.html */ "./src/app/components/agenda-creation/agenda-creation.component.html"),
            styles: [__webpack_require__(/*! ./agenda-creation.component.scss */ "./src/app/components/agenda-creation/agenda-creation.component.scss")]
        }),
        __metadata("design:paramtypes", [_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], AgendaCreationComponent);
    return AgendaCreationComponent;
}());



/***/ }),

/***/ "./src/app/components/agenda/agenda.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/agenda/agenda.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul>\r\n\t<li *ngFor=\"let agenda of state.agenda;\">\r\n\t\t<p (click)=\"setActive(agenda)\"><b>{{ agenda.name }}</b></p>\r\n\t\t<p *ngIf=\"agenda.active\">Description: {{ agenda.description }}</p>\r\n\t\t<p *ngIf=\"agenda.active\">Duration: {{ agenda.duration }}</p>\r\n\t\t<div *ngIf=\"agenda.active\">\r\n\t\t\t<label>Summary: </label>\r\n\t\t\t<input type=\"text\" name=\"Summary\" id=\"aDuration\" [(ngModel)]=\"agenda.summary.summary\" class=\"slds-input\"\r\n\t\t\t [placeholder]=\"\" />\r\n\t\t</div>\r\n\t\t<div *ngIf=\"agenda.active\">\r\n\t\t\t<label>Next Steps: </label>\r\n\t\t\t<input type=\"text\" name=\"Summary\" id=\"aDuration\" [(ngModel)]=\"agenda.summary.nextSteps\" class=\"slds-input\"\r\n\t\t\t [placeholder]=\"\" />\r\n\t\t</div>\r\n\t\t<div *ngIf=\"agenda.active\">\r\n\t\t\t<button (click)=\"markComplete(agenda)\">Completed</button>\r\n\t\t\t<button (click)=\"needsMoreDiscussion(agenda)\">Needs more discussion</button>\r\n\t\t</div>\r\n\t</li>\r\n</ul>"

/***/ }),

/***/ "./src/app/components/agenda/agenda.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/agenda/agenda.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWdlbmRhL2FnZW5kYS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/agenda/agenda.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/agenda/agenda.component.ts ***!
  \*******************************************************/
/*! exports provided: AgendaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaComponent", function() { return AgendaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AgendaComponent = /** @class */ (function () {
    function AgendaComponent(api) {
        this.api = api;
        this.state = api.state;
    }
    AgendaComponent.prototype.ngOnInit = function () {
    };
    AgendaComponent.prototype.setActive = function (agenda) {
        this.state.agenda.forEach(function (a) {
            if (a.guid === agenda.guid) {
                a.active = true;
            }
            else {
                a.active = false;
            }
        });
        console.log(agenda);
    };
    AgendaComponent.prototype.needsMoreDiscussion = function (agenda) {
        var found = false;
        var next;
        this.state.agenda.forEach(function (a) {
            if (found) {
                next = a;
                found = false;
            }
            if (a.guid === agenda.guid) {
                a.needsMoreDiscussion = true;
                a.active = false;
                found = true;
            }
        });
        if (next) {
            this.setActive(next);
        }
    };
    AgendaComponent.prototype.markComplete = function (agenda) {
        var found = false;
        var next;
        this.state.agenda.forEach(function (a) {
            if (found) {
                next = a;
                found = false;
            }
            if (a.guid === agenda.guid) {
                a.completed = true;
                found = true;
                a.active = false;
            }
        });
        if (next) {
            this.setActive(next);
        }
    };
    AgendaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-agenda',
            template: __webpack_require__(/*! ./agenda.component.html */ "./src/app/components/agenda/agenda.component.html"),
            styles: [__webpack_require__(/*! ./agenda.component.scss */ "./src/app/components/agenda/agenda.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], AgendaComponent);
    return AgendaComponent;
}());



/***/ }),

/***/ "./src/app/components/card-code/card-code.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/card-code/card-code.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">Code</div>\r\n"

/***/ }),

/***/ "./src/app/components/card-code/card-code.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/card-code/card-code.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1jb2RlL2NhcmQtY29kZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/card-code/card-code.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/card-code/card-code.component.ts ***!
  \*************************************************************/
/*! exports provided: CardCodeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardCodeComponent", function() { return CardCodeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/transmiter.service */ "./src/app/services/transmiter.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CardCodeComponent = /** @class */ (function () {
    function CardCodeComponent(transmiter) {
        this.transmiter = transmiter;
        this.subscription = transmiter.broadcastChannel$.subscribe(function (value) {
            console.log('CardCodeComponent recieved: ' + JSON.stringify(value));
        });
    }
    CardCodeComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    CardCodeComponent.prototype.ngOnInit = function () {
    };
    CardCodeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-code',
            template: __webpack_require__(/*! ./card-code.component.html */ "./src/app/components/card-code/card-code.component.html"),
            styles: [__webpack_require__(/*! ./card-code.component.scss */ "./src/app/components/card-code/card-code.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__["TransmiterService"]])
    ], CardCodeComponent);
    return CardCodeComponent;
}());



/***/ }),

/***/ "./src/app/components/card-drawing/card-drawing.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/card-drawing/card-drawing.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card draggable\">Drawing</div>"

/***/ }),

/***/ "./src/app/components/card-drawing/card-drawing.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/card-drawing/card-drawing.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1kcmF3aW5nL2NhcmQtZHJhd2luZy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/card-drawing/card-drawing.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/card-drawing/card-drawing.component.ts ***!
  \*******************************************************************/
/*! exports provided: CardDrawingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardDrawingComponent", function() { return CardDrawingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/transmiter.service */ "./src/app/services/transmiter.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CardDrawingComponent = /** @class */ (function () {
    function CardDrawingComponent(transmiter) {
        this.transmiter = transmiter;
        this.subscription = transmiter.broadcastChannel$.subscribe(function (value) {
            console.log('CardDrawingComponent recieved: ' + JSON.stringify(value));
        });
    }
    CardDrawingComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    CardDrawingComponent.prototype.ngOnInit = function () {
    };
    CardDrawingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-drawing',
            template: __webpack_require__(/*! ./card-drawing.component.html */ "./src/app/components/card-drawing/card-drawing.component.html"),
            styles: [__webpack_require__(/*! ./card-drawing.component.scss */ "./src/app/components/card-drawing/card-drawing.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__["TransmiterService"]])
    ], CardDrawingComponent);
    return CardDrawingComponent;
}());



/***/ }),

/***/ "./src/app/components/card-text/card-text.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/card-text/card-text.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">text</div>"

/***/ }),

/***/ "./src/app/components/card-text/card-text.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/card-text/card-text.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC10ZXh0L2NhcmQtdGV4dC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/card-text/card-text.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/card-text/card-text.component.ts ***!
  \*************************************************************/
/*! exports provided: CardTextComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardTextComponent", function() { return CardTextComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/transmiter.service */ "./src/app/services/transmiter.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CardTextComponent = /** @class */ (function () {
    function CardTextComponent(transmiter) {
        this.transmiter = transmiter;
        this.subscription = transmiter.broadcastChannel$.subscribe(function (value) {
            console.log('CardTextComponent recieved: ' + JSON.stringify(value));
        });
    }
    CardTextComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    CardTextComponent.prototype.ngOnInit = function () {
    };
    CardTextComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-card-text',
            template: __webpack_require__(/*! ./card-text.component.html */ "./src/app/components/card-text/card-text.component.html"),
            styles: [__webpack_require__(/*! ./card-text.component.scss */ "./src/app/components/card-text/card-text.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_transmiter_service__WEBPACK_IMPORTED_MODULE_1__["TransmiterService"]])
    ], CardTextComponent);
    return CardTextComponent;
}());



/***/ }),

/***/ "./src/app/components/chat-box/chat-box.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/chat-box/chat-box.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n    chat-box works!\r\n</p>\r\n\r\n<button (click)=\"sendMsg()\">Send Message</button>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/chat-box/chat-box.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/chat-box/chat-box.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2hhdC1ib3gvY2hhdC1ib3guY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/chat-box/chat-box.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/chat-box/chat-box.component.ts ***!
  \***********************************************************/
/*! exports provided: ChatBoxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatBoxComponent", function() { return ChatBoxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_websocket_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/websocket.service */ "./src/app/services/websocket.service.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/chat.service */ "./src/app/services/chat.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChatBoxComponent = /** @class */ (function () {
    function ChatBoxComponent(chatService) {
        this.chatService = chatService;
        this.message = {
            author: 'tutorialedge',
            message: 'this is a test message'
        };
        chatService.messages.subscribe(function (msg) {
            console.log("Response from websocket: " + msg);
        });
    }
    ChatBoxComponent.prototype.sendMsg = function () {
        console.log('new message from client to websocket: ', this.message);
        this.chatService.messages.next(this.message);
        this.message.message = '';
    };
    ChatBoxComponent.prototype.ngOnInit = function () {
    };
    ChatBoxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chat-box',
            template: __webpack_require__(/*! ./chat-box.component.html */ "./src/app/components/chat-box/chat-box.component.html"),
            styles: [__webpack_require__(/*! ./chat-box.component.scss */ "./src/app/components/chat-box/chat-box.component.scss")],
            providers: [_services_websocket_service__WEBPACK_IMPORTED_MODULE_1__["WebsocketService"], _services_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]]
        }),
        __metadata("design:paramtypes", [_services_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], ChatBoxComponent);
    return ChatBoxComponent;
}());



/***/ }),

/***/ "./src/app/components/kiki-rage/kiki-rage.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/kiki-rage/kiki-rage.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  kiki-rage works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/components/kiki-rage/kiki-rage.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/kiki-rage/kiki-rage.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMva2lraS1yYWdlL2tpa2ktcmFnZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/kiki-rage/kiki-rage.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/kiki-rage/kiki-rage.component.ts ***!
  \*************************************************************/
/*! exports provided: KikiRageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KikiRageComponent", function() { return KikiRageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var KikiRageComponent = /** @class */ (function () {
    function KikiRageComponent() {
    }
    KikiRageComponent.prototype.ngOnInit = function () {
    };
    KikiRageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-kiki-rage',
            template: __webpack_require__(/*! ./kiki-rage.component.html */ "./src/app/components/kiki-rage/kiki-rage.component.html"),
            styles: [__webpack_require__(/*! ./kiki-rage.component.scss */ "./src/app/components/kiki-rage/kiki-rage.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], KikiRageComponent);
    return KikiRageComponent;
}());



/***/ }),

/***/ "./src/app/components/main-header/main-header.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/main-header/main-header.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-header-left\">\r\n\t<div class=\"app-name\">Smart Office</div>\r\n\t<div class=\"meeting-name\">Design review session</div>\r\n</div>\r\n<div class=\"main-header-right\">\r\n\t<div class=\"presenter current\"></div>\r\n\t<img src=\"./assets/img/logo.svg\"/>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/main-header/main-header.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/main-header/main-header.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbWFpbi1oZWFkZXIvbWFpbi1oZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/main-header/main-header.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/main-header/main-header.component.ts ***!
  \*****************************************************************/
/*! exports provided: MainHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainHeaderComponent", function() { return MainHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainHeaderComponent = /** @class */ (function () {
    function MainHeaderComponent() {
    }
    MainHeaderComponent.prototype.ngOnInit = function () {
    };
    MainHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-header',
            template: __webpack_require__(/*! ./main-header.component.html */ "./src/app/components/main-header/main-header.component.html"),
            styles: [__webpack_require__(/*! ./main-header.component.scss */ "./src/app/components/main-header/main-header.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MainHeaderComponent);
    return MainHeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/users/users.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/users/users.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"remotes\" class=\"main-c videoContainer users-wrapper\">\r\n\t<div class=\"video-box\"><video id=\"selfVideo\" oncontextmenu=\"return false;\"></video></div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/users/users.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/users/users.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".videoContainer {\n  display: flex;\n  flex-direction: column; }\n\n.video-box {\n  width: 200px;\n  flex: 1 1 auto; }\n\n.video-box video {\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2Vycy9DOlxcVXNlcnNcXG5pa29sLmJhZGFuamFrXFxEb2N1bWVudHNcXFdvcmtcXEdpdFxcaGFja2l0bnBrZC9zcmNcXGFwcFxcY29tcG9uZW50c1xcdXNlcnNcXHVzZXJzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBWTtFQUNaLHVCQUFxQixFQUN4Qjs7QUFFRDtFQUNJLGFBQVc7RUFDWCxlQUFhLEVBQ2hCOztBQUVEO0VBQ0ksWUFBVSxFQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy91c2Vycy91c2Vycy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi52aWRlb0NvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjpjb2x1bW47IFxyXG59XHJcblxyXG4udmlkZW8tYm94IHtcclxuICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgZmxleDoxIDEgYXV0bztcclxufVxyXG5cclxuLnZpZGVvLWJveCB2aWRlbyB7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/components/users/users.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/users/users.component.ts ***!
  \*****************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _assets_WebCam_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../assets/WebCam.js */ "./src/assets/WebCam.js");
/* harmony import */ var _assets_WebCam_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_assets_WebCam_js__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UsersComponent = /** @class */ (function () {
    function UsersComponent() {
        console.log(_assets_WebCam_js__WEBPACK_IMPORTED_MODULE_1__);
        console.log(Object.keys(_assets_WebCam_js__WEBPACK_IMPORTED_MODULE_1__));
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/components/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/components/users/users.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/app/components/whiteboard/whiteboard.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/whiteboard/whiteboard.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"whiteboard-wrapper\">\r\n\t<div class=\"whiteboard-tabs\">\r\n\t\t<div class=\"tab active draggable\">Whiteboard</div>\r\n\t\t<div class=\"tab draggable\">Screenshare</div>\r\n\t\t<div class=\"tab draggable\">Code</div>\r\n\t\t<div class=\"tab draggable\">Image</div>\r\n\t\t<div class=\"tab draggable\">Video</div>\r\n\t</div>\r\n\t<div class=\"whiteboard-subtabs\">\r\n\t\t<div class=\"subtab draggable\">Text</div>\r\n\t\t<div class=\"subtab draggable\">Image</div>\r\n\t\t<div class=\"subtab draggable\">Draw panel</div>\r\n\t\t<div class=\"subtab draggable\">Code panel</div>\r\n\t\t<div class=\"subtab draggable\">Vote</div>\r\n\t\t<div class=\"subtab draggable\">Sticker</div>\r\n\t</div>\r\n\t<div class=\"whiteboard-panel\">\r\n\t\t<app-card-code></app-card-code>\r\n\t\t<app-card-drawing></app-card-drawing>\r\n\t\t<app-card-text></app-card-text>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/whiteboard/whiteboard.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/whiteboard/whiteboard.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2hpdGVib2FyZC93aGl0ZWJvYXJkLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/whiteboard/whiteboard.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/whiteboard/whiteboard.component.ts ***!
  \***************************************************************/
/*! exports provided: WhiteboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhiteboardComponent", function() { return WhiteboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var interactjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! interactjs */ "./node_modules/interactjs/dist/interact.js");
/* harmony import */ var interactjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(interactjs__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WhiteboardComponent = /** @class */ (function () {
    function WhiteboardComponent() {
    }
    WhiteboardComponent.prototype.ngOnInit = function () {
        interactjs__WEBPACK_IMPORTED_MODULE_1__('.draggable')
            .draggable({
            // enable inertial throwing
            inertia: true,
            // keep the element within the area of it's parent
            restrict: {
                restriction: ".whiteboard-wrapper",
                endOnly: true,
                elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
            },
            // enable autoScroll
            autoScroll: true,
            // call this function on every dragmove event
            onmove: dragMoveListener,
            // call this function on every dragend event
            onend: function (event) {
                var textEl = event.target.querySelector('p');
                textEl && (textEl.textContent =
                    'moved a distance of '
                        + (Math.sqrt(Math.pow(event.pageX - event.x0, 2) +
                            Math.pow(event.pageY - event.y0, 2) | 0))
                            .toFixed(2) + 'px');
            }
        });
        function dragMoveListener(event) {
            var target = event.target, 
            // keep the dragged position in the data-x/data-y attributes
            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx, y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
            // translate the element
            target.style.webkitTransform =
                target.style.transform =
                    'translate(' + x + 'px, ' + y + 'px)';
            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }
    };
    WhiteboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-whiteboard',
            template: __webpack_require__(/*! ./whiteboard.component.html */ "./src/app/components/whiteboard/whiteboard.component.html"),
            styles: [__webpack_require__(/*! ./whiteboard.component.scss */ "./src/app/components/whiteboard/whiteboard.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], WhiteboardComponent);
    return WhiteboardComponent;
}());



/***/ }),

/***/ "./src/app/model/Model.ts":
/*!********************************!*\
  !*** ./src/app/model/Model.ts ***!
  \********************************/
/*! exports provided: State, Agenda, MeetingMinutes, WhiteBoardItem, Comment, User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "State", function() { return State; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Agenda", function() { return Agenda; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MeetingMinutes", function() { return MeetingMinutes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhiteBoardItem", function() { return WhiteBoardItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Comment", function() { return Comment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var State = /** @class */ (function () {
    function State() {
        this.sessionId = 'Test';
        this.agenda = [];
        this.guid = this.generateGuid();
        this.showMeeting = false;
    }
    State.prototype.generateGuid = function () {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
    };
    State.prototype.s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    State.prototype.addAgenda = function (a) {
        this.agenda.push(a);
    };
    return State;
}());

var Agenda = /** @class */ (function () {
    function Agenda(name, description, duration) {
        this.active = false;
        this.completed = false;
        this.needsMoreDiscussion = false;
        this.name = name;
        this.description = description;
        this.guid = this.generateGuid();
        this.duration = duration;
        this.summary = new MeetingMinutes(this.guid, '', '');
    }
    Agenda.prototype.addItem = function (w) {
        this.items.push(w);
    };
    Agenda.prototype.deleteItem = function (guid) {
        var position = this.items.findIndex(function (item) {
            return item.guid === guid;
        });
        if (position > -1) {
            this.items = this.items.splice(position, 1);
        }
    };
    Agenda.prototype.generateGuid = function () {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
    };
    Agenda.prototype.s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    return Agenda;
}());

var MeetingMinutes = /** @class */ (function () {
    function MeetingMinutes(agenda, summary, nextSteps) {
        this.agenda = agenda;
        this.guid = this.generateGuid();
        this.summary = summary;
        this.nextSteps = nextSteps;
    }
    MeetingMinutes.prototype.generateGuid = function () {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
    };
    MeetingMinutes.prototype.s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    return MeetingMinutes;
}());

var WhiteBoardItem = /** @class */ (function () {
    function WhiteBoardItem(name, type) {
        this.guid = this.generateGuid();
        this.type = type;
        this.name = name;
        this.comments = [];
        this.vote = 0;
    }
    WhiteBoardItem.prototype.upvote = function () {
        this.vote += 1;
    };
    WhiteBoardItem.prototype.downvote = function () {
        this.vote -= 1;
    };
    WhiteBoardItem.prototype.addComment = function (c) {
        this.comments.push(c);
    };
    WhiteBoardItem.prototype.generateGuid = function () {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
    };
    WhiteBoardItem.prototype.s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    return WhiteBoardItem;
}());

var Comment = /** @class */ (function () {
    function Comment(text) {
        this.guid = this.generateGuid();
        this.text = text;
    }
    Comment.prototype.generateGuid = function () {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
    };
    Comment.prototype.s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    return Comment;
}());

var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/service/api.service.ts":
/*!****************************************!*\
  !*** ./src/app/service/api.service.ts ***!
  \****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_Model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/Model */ "./src/app/model/Model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ApiService = /** @class */ (function () {
    function ApiService() {
        this.state = new _model_Model__WEBPACK_IMPORTED_MODULE_1__["State"]();
    }
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/services/chat.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/chat.service.ts ***!
  \******************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _websocket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./websocket.service */ "./src/app/services/websocket.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CHAT_URL = 'ws://echo.websocket.org/';
var ChatService = /** @class */ (function () {
    function ChatService(wsService) {
        this.messages = wsService
            .connect(CHAT_URL)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (response) {
            var data = JSON.parse(response.data);
            return {
                author: data.author,
                message: data.message
            };
        }));
    }
    ChatService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_websocket_service__WEBPACK_IMPORTED_MODULE_2__["WebsocketService"]])
    ], ChatService);
    return ChatService;
}());



/***/ }),

/***/ "./src/app/services/transmiter.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/transmiter.service.ts ***!
  \************************************************/
/*! exports provided: TransmiterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransmiterService", function() { return TransmiterService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var TransmiterService = /** @class */ (function () {
    function TransmiterService() {
        this.source = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.broadcastChannel$ = this.source.asObservable();
    }
    TransmiterService.prototype.broadcast = function (value) {
        this.source.next(value);
    };
    TransmiterService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], TransmiterService);
    return TransmiterService;
}());



/***/ }),

/***/ "./src/app/services/websocket.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/websocket.service.ts ***!
  \***********************************************/
/*! exports provided: WebsocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsocketService", function() { return WebsocketService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WebsocketService = /** @class */ (function () {
    function WebsocketService() {
    }
    WebsocketService.prototype.connect = function (url) {
        if (!this.subject) {
            this.subject = this.create(url);
            console.log("Successfully connected: " + url);
        }
        return this.subject;
    };
    WebsocketService.prototype.create = function (url) {
        var ws = new WebSocket(url);
        var observable = rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"].create(function (obs) {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);
            return ws.close.bind(ws);
        });
        var observer = {
            next: function (data) {
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            }
        };
        return rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"].create(observer, observable);
    };
    WebsocketService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], WebsocketService);
    return WebsocketService;
}());



/***/ }),

/***/ "./src/assets/WebCam.js":
/*!******************************!*\
  !*** ./src/assets/WebCam.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Determine the room name and public URL for this chat session.
function getRoom() {
	var query = location.search && location.search.split("?")[1];

	if (query) {
		return (location.search && decodeURIComponent(query.split("=")[1]));
	}

	return 'test';
}

// Enable video on the page.
function enableVideo() {

	loadSimpleWebRTC();
}

// Render the login form.
function showLogin() {
	okta.renderEl({ el: "#okta-login-container" }, function (res) {
	}, function (err) {
		alert("Couldn't render the login form, something horrible must have happened. Please refresh the page.");
	});

	document.getElementById("login").style.display = "none";
}

// Determine whether or not we have a querystring.
function hasQueryString() {
	return location.href.indexOf("?") !== -1;
}

// Dynamically load the simplewebrtc script so that we can
// kickstart the video call.
function loadSimpleWebRTC() {

	var webrtc = new SimpleWebRTC({
		localVideoEl: "selfVideo",
		// the id/element dom element that will hold remote videos
		remoteVideosEl: "",
		autoRequestMedia: true,
		debug: false,
		detectSpeakingEvents: true,
		autoAdjustMic: false,
		nick: 'Kristijan'
	});

	// Immediately join room when loaded.
	webrtc.on("readyToCall", function () {
		webrtc.joinRoom(getRoom());
	});

	// If we didn't get access to the camera, raise an error.
	webrtc.on("localMediaError", function (err) {
		alert("This service only works if you allow camera access.Please grant access and refresh the page.");
	});

	// When another person joins the chat room, we'll display their video.
	webrtc.on("videoAdded", function (video, peer) {
		console.log("user added to chat", peer);
		var remotes = document.getElementById("remotes");

		if (remotes) {
			var outerContainer = document.createElement("div");
			outerContainer.className = "video-box";
			var container = document.createElement("div");
			container.className = "video-box";
			container.id = "container_" + webrtc.getDomId(peer);
			container.appendChild(video);
			
			// Suppress right-clicks on the video.
			video.oncontextmenu = function () {
				return false;
			};
			// Show the connection state.
			if (peer && peer.pc) {
				var connstate = document.createElement("div");
				connstate.className = "connectionstate";
				container.appendChild(connstate);

				peer.pc.on("iceConnectionStateChange", function (event) {
					switch (peer.pc.iceConnectionState) {
						case "checking":
							connstate.innerText = "connecting to peer...";
							break;
						case "connected":
						case "completed": // on caller side
							connstate.innerText = peer.nick;
							break;
						case "disconnected":
							connstate.innerText = "disconnected";
							break;
						case "failed":
							connstate.innerText = "connection failed";
							break;
						case "closed":
							connstate.innerText = "connection closed";
							break;
					}
				});
			}
			outerContainer.appendChild(container);
			remotes.appendChild(outerContainer);

			// If we're adding a new video we need to modify bootstrap so we
			// only get two videos per row.
			var remoteVideos = document.getElementById("remotes").getElementsByTagName("video").length;

			if (!(remoteVideos % 2)) {
				/*var spacer = document.createElement("div");
				spacer.className = "w-100";
				remotes.appendChild(spacer);*/
			}


		}
	});

	// If a user disconnects from chat, we need to remove their video feed.
	webrtc.on("videoRemoved", function (video, peer) {
		console.log("user removed from chat", peer);
		var remotes = document.getElementById("remotes");
		var el = document.getElementById("container_" + webrtc.getDomId(peer));
		if (remotes && el) {
			remotes.removeChild(el.parentElement);
		}
	});


	// If there is a P2P failure, we need to error out.
	webrtc.on("iceFailed", function (peer) {
		var connstate = document.querySelector("#container_" + webrtc.getDomId(peer) + " .connectionstate");
		console.log("local fail", connstate);
		if (connstate) {
			connstate.innerText = "connection failed";
			fileinput.disabled = "disabled";
		}
	});

	// remote p2p/ice failure
	webrtc.on("connectivityError", function (peer) {
		var connstate = document.querySelector("#container_" + webrtc.getDomId(peer) + " .connectionstate");
		console.log("remote fail", connstate);
		if (connstate) {
			connstate.innerText = "connection failed";
			fileinput.disabled = "disabled";
		}
	});

}
enableVideo();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\nikol.badanjak\Documents\Work\Git\hackitnpkd\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map