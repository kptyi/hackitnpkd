export class State {
	public sessionId: string;
	public agenda: Agenda[];
	public guid: string;
	public showMeeting: boolean;
	public userName: string;
	public meetingName: string = 'Video Conferencing';
	public items: WhiteBoardItem[];

	constructor() {
		this.sessionId = 'Test';
		this.agenda = [];
		this.guid = this.generateGuid();
		this.showMeeting = false;
		const a: Agenda = new Agenda('Introduction', 'What problems are we trying to solve', 2);
		const m1 = new MeetingMinutes(a.guid);
		m1.summary.push("One tool for meetings and collaboration");
		m1.summary.push("Ability to scale up in the future");
		m1.summary.push("Search, act and review previous meeting details");
		m1.summary.push("Share ideas, perform code review and vote on actions");
		m1.summary.push("No software required");
		a.summary = m1;
		a.active = true;
		a.completed = false;
		this.agenda.push(a);
		const a2: Agenda = new Agenda('Feature overview', 'What features this app will provide in the context of our problems', 2);
		const m2 = new MeetingMinutes(a.guid);
		m2.summary.push("Video communicaton");
		m2.summary.push("Collaborative whiteboard");
		m2.summary.push("Video capture and Image commenting");
		m2.summary.push("Real time code review");
		a2.summary = m2;
		a2.active = false;
		a2.completed = false;
		a2.closed = true;
		this.agenda.push(a2);
		const a3: Agenda = new Agenda('After the meeting', 'How to get the most out of video conferencing', 2);
		const m3 = new MeetingMinutes(a.guid);
		m3.summary.push("Meeting summary");
		m3.summary.push("Audio transcription");
		m3.summary.push("Follow up meeting");
		m3.nextSteps.push("Product Roadmap?");
		m3.nextSteps.push("Business plan");
		a3.summary = m3;
		a3.active = false;
		a3.closed = true;
		a3.completed = false;
		this.agenda.push(a3);
	}

	public addItem(w: WhiteBoardItem) {
		this.items.push(w);
	}

	public deleteItem(guid: string) {
		let position = this.items.findIndex((item) => {
			return item.guid === guid;
		});
		if (position > -1) {
			this.items = this.items.splice(position, 1);
		}
	}

	generateGuid(): string {
		return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
	}

	s4(): string {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}

	public addAgenda(a: Agenda) {
		this.agenda.push(a);
	}
}

export class Agenda {
	public name: string;
	public active: boolean = false;
	public description: string;
	public guid: string;
	public closed: boolean;
	public duration: number;
	public summary: MeetingMinutes;
	public completed: boolean = false;
	public needsMoreDiscussion: boolean = false;

	constructor(name: string, description: string, duration: number) {
		this.name = name;
		this.description = description;
		this.guid = this.generateGuid();
		this.duration = duration;
		this.summary = new MeetingMinutes(this.guid);
	}



	generateGuid(): string {
		return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
	}

	s4(): string {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}

}

export class MeetingMinutes {
	public agenda: string;
	public summary: string[];
	public guid: string;
	public nextSteps: string[];

	constructor(agenda: string) {
		this.agenda = agenda;
		this.guid = this.generateGuid();
		this.summary = [];
		this.nextSteps = [];
	}

	generateGuid(): string {
		return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
	}

	s4(): string {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
}

export class WhiteBoardItem {
	public type: string;
	public guid: string;
	public name: string;
	public value: string;
	public x: number;
	public y: number;
	public values: string[];
	public comments: Comment[];
	public vote: number;

	constructor(name: string, type: string) {
		this.guid = this.generateGuid();
		this.type = type;
		this.name = name;
		this.comments = [];
		this.vote = 0;
		this.values = [];
	}

	public upvote() {
		this.vote += 1;
	}

	public downvote() {
		this.vote -= 1;
	}

	public addComment(c: Comment) {
		this.comments.push(c);
	}

	generateGuid(): string {
		return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
	}

	s4(): string {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}

}

export class Comment {
	public text: string;
	public guid: string;

	constructor(text: string) {
		this.guid = this.generateGuid();
		this.text = text;
	}

	generateGuid(): string {
		return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
	}

	s4(): string {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
}

export class User {
	public name: string;
	public email: string;
}
