import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChild } from '@angular/core';
import { CardTextComponent } from '../card-text/card-text.component';
import { CardDrawingComponent } from '../card-drawing/card-drawing.component';
import { CardCodeComponent } from '../card-code/card-code.component';
import { CardImageComponent } from '../card-image/card-image.component';
import { CardVoteComponent } from '../card-vote/card-vote.component';
import { CardStickerComponent } from '../card-sticker/card-sticker.component';

import { Subscription } from 'rxjs';
import * as interact from 'interactjs';

import { DynamicDirectiveDirective } from '../../directives/dynamic-directive.directive';

import { TransmiterService } from '../../services/transmiter.service'

export interface IDynamicPlugin {
	data: any;
}

import { startListening } from '../../../assets/stt.js';
import { ApiService } from 'src/app/service/api.service.js';
declare var USER_NAME;

@Component({
	selector: 'app-whiteboard',
	templateUrl: './whiteboard.component.html',
	styleUrls: ['./whiteboard.component.scss']
})
export class WhiteboardComponent implements OnInit {

    // @ViewChild(DynamicDirectiveDirective) adHost: DynamicDirectiveDirective;

    @ViewChild("dynamicPlaceholder", {
        read: ViewContainerRef
    }) componentContainer: ViewContainerRef;
    
	subscription: Subscription;

	constructor(public api: ApiService, private componentFactoryResolver: ComponentFactoryResolver, private transmiter: TransmiterService) {

		USER_NAME = api.state.userName;

		this.subscription = transmiter.broadcastChannel$.subscribe(
			value => {
				console.log('WhiteboardComponent recieved: ' + JSON.stringify(value));

				if (value && value['component'] === 'ContainerComponent' && value['action'] === 'createComponent') {

					this.createComponent(value['value']);
				}
			}
		);
	}

	ngOnInit() {
		interact('.draggable')
			.draggable({
				// enable inertial throwing
				inertia: true,
				// keep the element within the area of it's parent
				restrict: {
					restriction: ".whiteboard-wrapper",
					endOnly: true,
					elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
				},
				// enable autoScroll
				autoScroll: true,

				// call this function on every dragmove event
				onmove: dragMoveListener,
				// call this function on every dragend event
				onend: function (event) {
				}
			});
        interact('.resizable').resizable({});
		function dragMoveListener(event) {
            var target = event.target;
            target = target.parentElement;
            target = target.parentElement;
				// keep the dragged position in the data-x/data-y attributes
			var	x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
			var	y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
            console.log(target);
            // translate the element
            
			target.style.webkitTransform =
				target.style.transform =
				'translate(' + x + 'px, ' + y + 'px)';

			// update the posiion attributes
			target.setAttribute('data-x', x);
			target.setAttribute('data-y', y);
		}
		startListening();
	}

	createComponent(componentName: string): void {

		let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.getcomponent(componentName));
        let componentRef = this.componentContainer.createComponent(componentFactory);
	}

	private getcomponent(componentName: string): any {

		if (componentName === 'Text') {
			return CardTextComponent;
		} else if (componentName === 'Draw panel') {
			return CardDrawingComponent;
		} else if (componentName === 'Code panel') {
			return CardCodeComponent;
        } else if (componentName === 'Vote') {
            return CardVoteComponent;
        } else if (componentName === 'Sticker') {
            return CardStickerComponent;
        } else if (componentName === 'Image') {
            return CardImageComponent;
        }
	}
}
