import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'

@Component({
    selector: 'app-card-code',
    templateUrl: './card-code.component.html',
    styleUrls: ['./card-code.component.scss']
})
export class CardCodeComponent implements OnInit {

    subscription: Subscription;

    constructor(private transmiter: TransmiterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardCodeComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }
}
