import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	public userName: string;
	constructor(public api: ApiService) { }

	ngOnInit() {
	}

	public next() {
		console.log('KK' + ' ' + this.userName);
		this.api.state.userName = this.userName;
        this.api.state.showMeeting = true;

        window['logedinUser'] = this.userName;
	}

}
