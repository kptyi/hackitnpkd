import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { State, Agenda, MeetingMinutes } from 'src/app/model/Model';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'

@Component({
	selector: 'app-agenda',
	templateUrl: './agenda.component.html',
	styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {
	public state: State;
	public summary: MeetingMinutes;
	public tmpSumInpt: string;
    public tmpStpInpt: string;
    subscription: Subscription;

    public totalLikes: number = 0;
    public totalDislikes: number = 0;

    constructor(public api: ApiService, private transmiter: TransmiterService) {
        this.state = api.state;

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardTextComponent recieved: ' + JSON.stringify(value));

                if (value && value['component'] === 'RatingComponent' && value['action'] === 'dislike') {
                    this.totalDislikes--;
                } else if (value && value['component'] === 'RatingComponent' && value['action'] === 'like') {
                    this.totalLikes++;
                }
            }
        );
	}

	ngOnInit() {
	}

	onKeydown(event, agenda: Agenda) {
		agenda.summary.summary.push(this.tmpSumInpt);
		this.tmpSumInpt = '';
	}

	onKeydownSteps(event, agenda: Agenda) {
		agenda.summary.nextSteps.push(this.tmpStpInpt);
		this.tmpStpInpt = '';
	}

	setActive(agenda: Agenda) {
		this.state.agenda.forEach((a) => {
			if (a.guid === agenda.guid) {
				a.active = true;
				a.closed = false;
			} else {
				a.active = false;
				a.closed = true;
			}
		})
		console.log(agenda);
	}

	needsMoreDiscussion(agenda: Agenda) {
		let found = false;
		let next: Agenda;
		this.state.agenda.forEach((a) => {
			if (found) {
				next = a;
				found = false;
			}
			if (a.guid === agenda.guid) {
				a.needsMoreDiscussion = true;
				a.active = false;
				found = true;
			}
		});
		if (next) {
			this.setActive(next);
		}
	}

	markComplete(agenda: Agenda) {
		let found = false;
		let next: Agenda;
		agenda.closed = true;
		this.state.agenda.forEach((a) => {
			if (found) {
				next = a;
				found = false;
			}
			if (a.guid === agenda.guid) {
				a.completed = true;
				found = true;
				a.active = false;
			}
		});
		if (next) {
			this.setActive(next);
		}
	}

}
