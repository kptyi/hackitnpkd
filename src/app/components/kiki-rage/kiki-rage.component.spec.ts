import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KikiRageComponent } from './kiki-rage.component';

describe('KikiRageComponent', () => {
  let component: KikiRageComponent;
  let fixture: ComponentFixture<KikiRageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KikiRageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KikiRageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
