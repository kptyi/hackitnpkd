import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-image',
	templateUrl: './image.component.html',
	styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

	constructor() { }

	ngOnInit() {
		document.querySelector('#imgInput').addEventListener('change', function () {
			if (this.files && this.files[0]) {
				var img = document.getElementById('imgUpload');
				img['src'] = URL.createObjectURL(this.files[0]);
			}
		});
	}
}
