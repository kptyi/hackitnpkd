import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'

@Component({
    selector: 'app-card-drawing',
    templateUrl: './card-drawing.component.html',
    styleUrls: ['./card-drawing.component.scss']
})
export class CardDrawingComponent implements OnInit {

    subscription: Subscription;

    constructor(private transmiter: TransmiterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardDrawingComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }
}
