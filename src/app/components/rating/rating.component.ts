import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'

@Component({
    selector: 'app-rating',
    templateUrl: './rating.component.html',
    styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

    totalLikes: number = 0;
    totalDislikes: number = 0;
    chatMessage: string = '';
    toogleComments: boolean = false;

    subscription: Subscription;

    constructor(private transmiter: TransmiterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardCodeComponent recieved: ' + JSON.stringify(value));
            }
        );
    }

    ngOnInit() {

    }

    like() {
        this.totalLikes++;
        this.transmiter.broadcast({ "component": 'RatingComponent', 'action': "like", "value": 1 });
    }

    dislike() {
        this.totalDislikes--;
        this.transmiter.broadcast({ "component": 'RatingComponent', 'action': "dislike", "value": -1 });
    }

    sendMsg() {

        if (!this.chatMessage) {
            return;
        }

        let msg = {
            author: window['logedinUser'],
            message: this.chatMessage
        }

        this.transmiter.broadcast({ "component": 'RatingComponent', 'action': "comment", "value": msg });
        this.toogleComments = false;
        this.chatMessage = '';
    }

    toogleCommentBlock() {

        this.toogleComments = true;
    }
}
