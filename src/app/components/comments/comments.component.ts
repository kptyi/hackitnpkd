import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../../services/websocket.service'
import { ChatService } from '../../services/chat.service';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'

@Component({
    selector: 'app-comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

    subscription: Subscription;

    user: string = window['logedinUser'];
    messages: any[] = [];

    constructor(private transmiter: TransmiterService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardDrawingComponent recieved: ' + JSON.stringify(value));

                if (value && value['component'] === 'RatingComponent' && value['action'] === 'comment') {

                    this.messages.push(value['value']);
                }
            }
        );
    }

    ngOnInit() {

    }
}
