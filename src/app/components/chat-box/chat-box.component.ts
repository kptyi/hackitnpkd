import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../../services/websocket.service'
import { ChatService } from '../../services/chat.service';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'

@Component({
    selector: 'app-chat-box',
    templateUrl: './chat-box.component.html',
    styleUrls: ['./chat-box.component.scss'],
    providers: [WebsocketService, ChatService]
})
export class ChatBoxComponent implements OnInit {

    subscription: Subscription;

    user: string = window['logedinUser'];
    messages: any[] = [];
    chatMessage: string = '';
    toogleComments: boolean = false;

    messagesMock: any[] = [];

    constructor(private chatService: ChatService, private transmiter: TransmiterService) {
        chatService.messages.subscribe(msg => {
            console.log("Response from websocket: " + msg);
        });

        //this.subscription = transmiter.broadcastChannel$.subscribe(
        //    value => {
        //        console.log('CardDrawingComponent recieved: ' + JSON.stringify(value));

        //        if (value && value['component'] === 'RatingComponent' && value['action'] === 'comment') {

        //            this.messages.push(value['value']);
        //        }
        //    }
        //);

        this.messagesMock = [
            {
                "author": "Nikol",
                "message": "Jednostavan i ugodan UX"
            },
            {
                "author": "Petar",
                "message": "Automatsko prepoznavanje govora"
            },
            {
                "author": "Denis",
                "message": "Dinamicki board"
            },
            {
                "author": "Denis",
                "message": "Chat support"
            },
            {
                "author": "Denis",
                "message": "Ola"
            },
            {
                "author": "Petar",
                "message": "Pozdrav"
            },
            {
                "author": "Nikol",
                "message": "Pozdrav svima"
            }
        ];
    }

    sendMsg() {

        if (!this.chatMessage) {

            return;
        }

        let msg = {
            author: window['logedinUser'],
            message: this.chatMessage
        }

        console.log('new message from client to websocket: ', msg);

        // this.chatService.messages.next(msg);

        this.messages.push(msg);
        this.chatMessage = '';
    }

    ngOnInit() {
        console.log('ngOnInit...');
        var myVar = setInterval(myTimer, 2500);

        let that = this;
        function myTimer() {
            console.log('Timer...');
            if (that.messagesMock.length > 0) {
                that.messages.push(that.messagesMock[that.messagesMock.length - 1]);

                that.messagesMock.length = that.messagesMock.length - 1;
            }

            myStopFunction();
        }

        function myStopFunction() {

            if (that.messagesMock.length === 0) {
                clearInterval(myVar);
            }
        }
    }
}
