import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'
import { ApiService } from 'src/app/service/api.service';
import { WhiteBoardItem } from 'src/app/model/Model';

@Component({
    selector: 'app-card-image',
    templateUrl: './card-image.component.html',
    styleUrls: ['./card-image.component.scss']
})
export class CardImageComponent implements OnInit {

    subscription: Subscription;
    public tmpInpt: string;
    public item: WhiteBoardItem = new WhiteBoardItem('test', 'text');
    constructor(public viewContainerRef: ViewContainerRef, private transmiter: TransmiterService, public api: ApiService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardTextComponent recieved: ' + JSON.stringify(value));
                this.api.state.addItem(this.item);
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    onKeydown(event, item: WhiteBoardItem) {
        item.values.push(this.tmpInpt);
        this.tmpInpt = '';
    }

    ngOnInit() {
		document.querySelector('#imgInput2').addEventListener('change', function () {
			if (this.files && this.files[0]) {
				var img = document.getElementById('imgUpload2');
				img['src'] = URL.createObjectURL(this.files[0]);
			}
		});
    }

}
