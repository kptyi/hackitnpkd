import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { Agenda, State, MeetingMinutes } from '../../model/Model';

@Component({
	selector: 'app-agenda-creation',
	templateUrl: './agenda-creation.component.html',
	styleUrls: ['./agenda-creation.component.scss']
})
export class AgendaCreationComponent implements OnInit {
	public agendas: Agenda[];
	public duration: number;

	public name: string;
	public description: string;
	public state: State;
	constructor(api: ApiService) {
		this.state = api.state;
		console.log(this.state);
		console.log('KK');
		this.agendas = [];
	}

	ngOnInit() {
	}

	public addAgenda() {
		this.agendas.push(new Agenda(this.name, this.description, this.duration));
		this.name = '';
		this.duration = null;
		this.description = '';
	}

	public next() {
		this.agendas.forEach((agenda) => {
			this.state.agenda.push(agenda);
		});
		if (this.state.agenda.length > 0) {
			this.state.agenda[0].active = true;
		}
		this.state.showMeeting = true;
	}

}
