import { Component, OnInit } from '@angular/core';
import { loadSimpleWebRTC } from '../../../assets/WebCam.js';
import { ApiService } from 'src/app/service/api.service.js';
declare var webrtc;
import getEmail from '../../../assets/smtp.js';
let Email = getEmail();
@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
	public name: string;
	constructor(public api: ApiService) {
		this.name = this.api.state.userName;
	}

	ngOnInit() {
		webrtc = loadSimpleWebRTC({ name: this.name });
		console.log('sending email');
		console.log(Email.send);
		/*Email.send("nkdphackathon@gmail.com",
		"dns.kosutic@gmail.com",
		"This is a subject",
		"this is the body",
		"smtp.gmail.com",
		"nkdphackathon@gmail.com",
		"hackathon2018");*/
	}

}
