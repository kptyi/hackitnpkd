import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardStickerComponent } from './card-sticker.component';

describe('CardStickerComponent', () => {
  let component: CardStickerComponent;
  let fixture: ComponentFixture<CardStickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardStickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardStickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
