import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';

import { TransmiterService } from '../../services/transmiter.service'
import { ApiService } from 'src/app/service/api.service';
import { WhiteBoardItem } from 'src/app/model/Model';

@Component({
    selector: 'app-card-sticker',
    templateUrl: './card-sticker.component.html',
    styleUrls: ['./card-sticker.component.scss']
})
export class CardStickerComponent implements OnInit {

    subscription: Subscription;
    public tmpInpt: string;
    public item: WhiteBoardItem = new WhiteBoardItem('test', 'text');
    constructor(public viewContainerRef: ViewContainerRef, private transmiter: TransmiterService, public api: ApiService) {

        this.subscription = transmiter.broadcastChannel$.subscribe(
            value => {
                console.log('CardTextComponent recieved: ' + JSON.stringify(value));
                this.api.state.addItem(this.item);
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    onKeydown(event, item: WhiteBoardItem) {
        item.values.push(this.tmpInpt);
        this.tmpInpt = '';
    }

    ngOnInit() {

    }

}
