import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';

@Component({
	selector: 'app-video',
	templateUrl: './video.component.html',
	styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
	@ViewChild("video")
	public video: ElementRef;

	@ViewChild("canvas")
    public canvas: ElementRef;
    public visible: boolean = false;

    public captures: Array<any> = [];

	constructor() { }

	ngOnInit() {
	}

	public ngAfterViewInit() {
		if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
			let deviceId = '';
			navigator.mediaDevices.enumerateDevices()
				.then(devices => devices.filter(device => device.kind === 'videoinput' && device.label.indexOf('Rear') !== -1))
				.then(backFacingDevices => {

					console.log(backFacingDevices.map(device => {
						deviceId = device.deviceId;
						return device.deviceId;
					}));
				});
			const constraints = { audio: false, video: true };
			navigator.mediaDevices.getUserMedia(constraints).then(stream => {

				this.video.nativeElement.src = window.URL.createObjectURL(stream);
				this.video.nativeElement.play();
			});
		}
	}

	public capture() {
		var context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 640, 480);
        this.captures.push(this.canvas.nativeElement.toDataURL("image/png"));
        this.visible = true;
	}

}
