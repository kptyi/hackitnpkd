import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { State } from 'src/app/model/Model';

import { Subscription } from 'rxjs';

@Component({
    selector: 'app-main-header',
    templateUrl: './main-header.component.html',
    styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {

    public state: State;
    subscription: Subscription;

    constructor(public api: ApiService) {

        this.state = api.state;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {

    }

}
