import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChild } from '@angular/core';

import { TransmiterService } from '../../services/transmiter.service'

@Component({
    selector: 'app-container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

    activeComponent: string = 'Whiteboard';

    constructor(private transmiter: TransmiterService) {

    }

    ngOnInit() {

    }

    showComponent(componentName: string) {

        this.activeComponent = componentName;
    }

    public createComponent(componentName: string): void {

        this.transmiter.broadcast({ "component": 'ContainerComponent', 'action': "createComponent", "value": componentName });
    }

}

