import { Component, ElementRef, Inject } from '@angular/core';
import { TransmiterService } from './services/transmiter.service';
import { ApiService } from './service/api.service';
declare var APP_DATA;
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [TransmiterService]
})
export class AppComponent {
	firstUser: boolean = false;
	title = 'my-sassy-app';
	showMeeting = this.api.state.showMeeting;

	constructor(private transmiter: TransmiterService, public api: ApiService) {
		console.log('KK data');
		console.log(APP_DATA);
		if (APP_DATA && APP_DATA !== '') {
			this.firstUser = true;
		}
		transmiter.broadcastChannel$.subscribe(
			value => {
				console.log('AppComponent: ' + value);
			});
	}

	raiseEvent() {

		this.transmiter.broadcast('Ok go');
	}
}

