import { Injectable } from '@angular/core';
import { State, Agenda } from '../model/Model';

@Injectable({
	providedIn: 'root'
})
export class ApiService {
	public state: State;

	constructor() {
		this.state = new State();
		window.document.addEventListener('DataRequest', (evnt) => {
			console.log('event');
			return evnt['detail']['response'](this.state.userName);
		});
	}
}
