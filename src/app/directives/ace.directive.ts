import { Directive, ElementRef } from '@angular/core';

declare var ace: any;

@Directive({
    selector: '[appAce]'
})
export class AceDirective {

    private editor;

    set text(s: string) {
        this.editor.setValue(s);
        this.editor.clearSelection();
        this.editor.focus();
    }

    constructor(elementRef: ElementRef) {

        let el = elementRef.nativeElement;
        el.classList.add("aceEditor");

        this.editor = ace.edit(el);
        this.editor.resize(false);
        this.editor.setTheme("ace/theme/monokai");
        this.editor.getSession().setMode("ace/mode/javascript");
        this.editor.$blockScrolling = 1;

        this.editor.on("change", (e) => {
            let tmp = this.editor.getValue();
        });
    }

    ngOnInit() {

    }
}
