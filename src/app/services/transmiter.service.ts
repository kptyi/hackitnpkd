import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class TransmiterService {

	public source = new Subject<string>();

	public broadcastChannel$ = this.source.asObservable();

	broadcast(value: any) {
		this.source.next(value);
	}
}
