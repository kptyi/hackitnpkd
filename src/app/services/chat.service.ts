import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { WebsocketService } from './websocket.service';

const CHAT_URL = 'ws://chatboxhackaton.herokuapp.com:3000';

export interface Message {
    author: string,
    message: string
}

@Injectable()
export class ChatService {
    public messages: Subject<Message>;

    constructor(wsService: WebsocketService) {

        this.messages = <Subject<Message>>wsService
            .connect(CHAT_URL)
            .pipe(map((response: MessageEvent): Message => {
                console.log('aaaaaaaaaaaaaaaaaaaaaa');
                let data = JSON.parse(response.data);
                return {
                    author: data.author,
                    message: data.message
                }
            }));
    }
}