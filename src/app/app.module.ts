import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ChatBoxComponent } from './components/chat-box/chat-box.component';
import { MainHeaderComponent } from './components/main-header/main-header.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { WhiteboardComponent } from './components/whiteboard/whiteboard.component';
import { UsersComponent } from './components/users/users.component';
import { KikiRageComponent } from './components/kiki-rage/kiki-rage.component';
import { AgendaCreationComponent } from './components/agenda-creation/agenda-creation.component';
import { ApiService } from './service/api.service';

import { CardCodeComponent } from './components/card-code/card-code.component';
import { CardDrawingComponent } from './components/card-drawing/card-drawing.component';
import { CardTextComponent } from './components/card-text/card-text.component';
import { TransmiterService } from './services/transmiter.service';
import { AceDirective } from './directives/ace.directive'
import { LoginComponent } from './components/login/login.component';
import { ContainerComponent } from './components/container/container.component';
import { ScreenshareComponent } from './components/screenshare/screenshare.component';
import { CodeComponent } from './components/code/code.component';
import { ImageComponent } from './components/image/image.component';
import { VideoComponent } from './components/video/video.component';
import { DynamicDirectiveDirective } from './directives/dynamic-directive.directive';
import { CommentsComponent } from './components/comments/comments.component';
import { RatingComponent } from './components/rating/rating.component';
import { CardImageComponent } from './components/card-image/card-image.component';
import { CardVoteComponent } from './components/card-vote/card-vote.component';
import { CardStickerComponent } from './components/card-sticker/card-sticker.component'

@NgModule({
	declarations: [
		AppComponent,
		ChatBoxComponent,
		MainHeaderComponent,
		AgendaComponent,
		WhiteboardComponent,
		UsersComponent,
		KikiRageComponent,
		AgendaCreationComponent,
		CardCodeComponent,
		CardDrawingComponent,
		CardTextComponent,
		AceDirective,
		LoginComponent,
		ContainerComponent,
		ScreenshareComponent,
		CodeComponent,
		ImageComponent,
		VideoComponent,
		DynamicDirectiveDirective,
		CommentsComponent,
		RatingComponent,
		CardImageComponent,
		CardVoteComponent,
		CardStickerComponent
	],
	imports: [
		BrowserModule,
		FormsModule
	],
	providers: [ApiService, { provide: 'AppData', useValue: (<any>window).APP_DATA }],
    bootstrap: [AppComponent],
    entryComponents: [CardTextComponent, CardDrawingComponent, CardCodeComponent, CardImageComponent, CardVoteComponent, CardStickerComponent]
})
export class AppModule { }
