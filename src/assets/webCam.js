var request = function (action, payload) {
	if (!action) {
		throw new Error('Either name or payload not provided!');
	}
	return new Promise((res, rej) => {
		const resolve = (result, err) => {
			if (!err) {
				res(result);
			} else {
				rej(err, 'Unable to perform api request', new Error('Unable to perform api request'));
			}
		};
		const event = new CustomEvent('DataRequest', {
			detail: {
				request: action,
				parameters: payload,
				response: resolve
			}

		});
		window.document.dispatchEvent(event);
	});
};
// Determine the room name and public URL for this chat session.
function getRoom() {
	var query = location.search && location.search.split("?")[1];
	return 'hackaton2018';
	if (query) {
		return (location.search && decodeURIComponent(query.split("=")[1]));
	}

	return 'hackaton2018';
}

// Render the login form.
function showLogin() {
	okta.renderEl({ el: "#okta-login-container" }, function (res) {
	}, function (err) {
		alert("Couldn't render the login form, something horrible must have happened. Please refresh the page.");
	});

	document.getElementById("login").style.display = "none";
}

// Determine whether or not we have a querystring.
function hasQueryString() {
	return location.href.indexOf("?") !== -1;
}

// Dynamically load the simplewebrtc script so that we can
// kickstart the video call.
function loadSimpleWebRTC(params) {
	return request('getUserName', '').then((username) => {
		webrtc = new SimpleWebRTC({
			localVideoEl: "selfVideo",
			// the id/element dom element that will hold remote videos
			remoteVideosEl: "",
			autoRequestMedia: true,
			debug: false,
			detectSpeakingEvents: true,
			autoAdjustMic: false,
			nick: username
		});

		// Immediately join room when loaded.
		webrtc.on("readyToCall", function () {
			webrtc.joinRoom(getRoom());
		});

		// If we didn't get access to the camera, raise an error.
		webrtc.on("localMediaError", function (err) {
			alert("This service only works if you allow camera access.Please grant access and refresh the page.");
		});

		// When another person joins the chat room, we'll display their video.
		webrtc.on("videoAdded", function (video, peer) {
			console.log("user added to chat", peer);
			var remotes = document.getElementById("remotes");

			if (remotes) {
				var outerContainer = document.createElement("div");
				outerContainer.className = "video-box";
				var container = document.createElement("div");
				container.className = "video-box";
				container.id = "container_" + webrtc.getDomId(peer);
				container.appendChild(video);

				// Suppress right-clicks on the video.
				video.oncontextmenu = function () {
					return false;
				};
				// Show the connection state.
				if (peer && peer.pc) {
					var connstate = document.createElement("div");
					connstate.className = "connectionstate";
					container.appendChild(connstate);

					peer.pc.on("iceConnectionStateChange", function (event) {
						switch (peer.pc.iceConnectionState) {
							case "checking":
								connstate.innerText = "connecting to peer...";
								break;
							case "connected":
							case "completed": // on caller side
								connstate.innerText = peer.nick;
								break;
							case "disconnected":
								connstate.innerText = "disconnected";
								break;
							case "failed":
								connstate.innerText = "connection failed";
								break;
							case "closed":
								connstate.innerText = "connection closed";
								break;
						}
					});
				}
				outerContainer.appendChild(container);
				remotes.appendChild(outerContainer);

				// If we're adding a new video we need to modify bootstrap so we
				// only get two videos per row.
				var remoteVideos = document.getElementById("remotes").getElementsByTagName("video").length;

				if (!(remoteVideos % 2)) {
					/*var spacer = document.createElement("div");
					spacer.className = "w-100";
					remotes.appendChild(spacer);*/
				}


			}
		});

		// If a user disconnects from chat, we need to remove their video feed.
		webrtc.on("videoRemoved", function (video, peer) {
			console.log("user removed from chat", peer);
			var remotes = document.getElementById("remotes");
			var el = document.getElementById("container_" + webrtc.getDomId(peer));
			if (remotes && el) {
				remotes.removeChild(el.parentElement);
			}
		});


		// If there is a P2P failure, we need to error out.
		webrtc.on("iceFailed", function (peer) {
			var connstate = document.querySelector("#container_" + webrtc.getDomId(peer) + " .connectionstate");
			console.log("local fail", connstate);
			if (connstate) {
				connstate.innerText = "connection failed";
				fileinput.disabled = "disabled";
			}
		});

		// remote p2p/ice failure
		webrtc.on("connectivityError", function (peer) {
			var connstate = document.querySelector("#container_" + webrtc.getDomId(peer) + " .connectionstate");
			console.log("remote fail", connstate);
			if (connstate) {
				connstate.innerText = "connection failed";
				fileinput.disabled = "disabled";
			}
		});

		webrtc.on('channelMessage', function (peer, channelLabel, data) {
			if (data.type === 'message') {
				console.log(data);
				var noteTextarea = jQuery('#note-textarea');
				var noteContent = noteTextarea.val();
				noteContent += '\n' + data.payload.username + ': ' + data.payload.data;
				noteTextarea.val(noteContent);
			}
		});

		return Promise.resolve(webrtc);
	});


}
export { loadSimpleWebRTC };